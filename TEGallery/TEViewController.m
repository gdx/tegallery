//
//  TEViewController.m
//  TEGallery
//
//  Created by  on 12/1/9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "TEViewController.h"
#import "AFUIImageReflection.h"


@implementation TEViewController

@synthesize gallery = _gallery;

- (NSInteger)numbersOfPhotoWithGallery:(TEGallery *)gallery {
    return [_cards count];
}

- (TEPhoto *)gallery:(TEGallery *)gallery photoAtIndex:(NSInteger)index {
    return [TEPhoto photoWithImage:[_cards objectAtIndex:index]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (void)gallery:(TEGallery *)gallery didSelectPhotoAtIndex:(NSInteger)index {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:[NSString stringWithFormat:@"You clicked on %d", index]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _cards = [NSArray arrayWithObjects:
              [[UIImage imageNamed:@"home_card_shinytea"] addImageReflection:1.0f 
                                                                     spacing:5.0f], 
              [[UIImage imageNamed:@"home_card_shinytea"] addImageReflection:1.0f 
                                                                     spacing:5.0f], 
              [[UIImage imageNamed:@"home_card_shinytea"] addImageReflection:1.0f 
                                                                     spacing:5.0f], 
              [[UIImage imageNamed:@"home_card_shinytea"] addImageReflection:1.0f 
                                                                     spacing:5.0f], 
              [[UIImage imageNamed:@"home_card_shinytea"] addImageReflection:1.0f 
                                                                     spacing:5.0f], 
              [[UIImage imageNamed:@"home_card_shinytea"] addImageReflection:1.0f 
                                                                     spacing:5.0f], 
              nil];
    [_cards retain];
    self.gallery.photoSize = CGSizeMake(200, 200);
    self.gallery.scaleEnabled = YES;
    self.gallery.dataSource = self;
    self.gallery.delegate = self;
    [self.gallery reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.gallery = nil;
    [_cards release];
    _cards = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
